function wait(time){
    setTimeout(() => {
        console.log('function executes after: ' + time + ' ms');
    }, time);
}
wait(2000)