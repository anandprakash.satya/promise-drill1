let secondPromise = new Promise((resolve,reject)=>{
    resolve('Second!')
})
let firstPromise = new Promise((resolve,reject)=>{
    resolve(secondPromise)
})
firstPromise.then((value)=>{
    console.log(value);
})