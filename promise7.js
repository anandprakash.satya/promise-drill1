let promise1=new Promise((resolve, reject) => {
    resolve('promise1')
})
let promise2=new Promise((resolve, reject) => {
    reject('promise2')
})
let promise3=new Promise((resolve, reject) => {
    resolve('promise3')
})
let promise4=new Promise((resolve, reject) => {
    resolve('promise4')
})
Promise.all([promise1,promise2,promise3,promise4])
.then((value)=>console.log(value))
.catch((err)=>console.log(err))
