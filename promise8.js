const fetch = require('node-fetch');

fetch('https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json')

.then((data)=>data.json())
.then((data)=>console.log(data.houses.map((elem)=>{
    return elem.name
})))
.catch(()=>console.log('user is not connected to internet'))